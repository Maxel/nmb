walk = document.createTreeWalker(document.documentElement, NodeFilter.SHOW_TEXT, null, false);
while(n = walk.nextNode()) {
    text = n.textContent;
    if (!text) continue;
    date = new Date(text);
    if (date.toString() === "Invalid Date") continue;
    date.setMonth(date.getMonth() - 9);
    n.textContent = text + " (" + date.toLocaleDateString("fr-FR", {year: 'numeric', month: 'long', day: 'numeric'}) + ")";
}
